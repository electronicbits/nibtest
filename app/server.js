// server.js
//displays Hello World

// modules =================================================
var express        = require('express');
var app            = express();

// configuration ===========================================
app.get('/', function (req, res) {
    res.send('Hello World!');
});

// expose app
exports = module.exports = app;