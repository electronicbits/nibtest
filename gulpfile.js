var gulp = require('gulp');
const mocha = require('gulp-mocha');
var jshint = require('gulp-jshint');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('app/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('mocha', function(){
    return gulp.src('./test/functional/landing.js', {read: false})
        .pipe(mocha({reporter: 'nyan'}))
        .once('error', function(){
            process.exit(1);
        })
        .once('end', function(){
            process.exit();
        });
});

gulp.task('default', ['lint', 'mocha']);